#include "dbpi.h"

#ifdef DEBUG
const int buttonLedPin =  13;   // LED indicating the button state
const int switchLedPin = 12;    // LED indicating the switch state
#endif

DBPI::DBPI(int _pin,
           int _pull,
           volatile bool &_switchState,
           bool _latching) :
      pull(_pull),
      pin(_pin),
      switchState(_switchState),
      interruptID(digitalPinToInterrupt(_pin)),
      latching (_latching) {
      buttonState = (pull == DEBOUNCE_PULLUP ? HIGH : LOW);

      
      #ifdef DEBUG
      pinMode(buttonLedPin, OUTPUT);
      pinMode(switchLedPin, OUTPUT);
      digitalWrite(buttonLedPin, LOW);

      Serial.print("pin: ");
      Serial.println(pin);
      Serial.print("pull: ");
      Serial.println(pull);
      Serial.print("interruptID: ");
      Serial.println(interruptID);
      #endif
      
      pinMode(pin, pull == DEBOUNCE_PULLUP ? INPUT_PULLUP : INPUT);
      Register(interruptID,this);
      attachInterrupt(interruptID,
                      ISRMethodVec[interruptID][pull == DEBOUNCE_PULLUP ? F_INDEX : R_INDEX],
                      pull == DEBOUNCE_PULLUP ? FALLING : RISING);      
    }
	
void DBPI::ISR_R(void){
  long currentTime = millis();
  
  if ((currentTime - lastDebounceTime) > debounceDelay){
    lastDebounceTime = currentTime;
    
    if (buttonState == LOW)  {
      buttonState = HIGH;
      attachInterrupt(interruptID, ISRMethodVec[interruptID][F_INDEX],FALLING);
    }
    #ifdef DEBUG
    digitalWrite(buttonLedPin, buttonState);
    #endif
    Switch(buttonState);
  }
}

 void DBPI::ISR_F(void){
  long currentTime = millis();
  
  if ((currentTime - lastDebounceTime) > debounceDelay){
    lastDebounceTime = currentTime;
    
    if (buttonState == HIGH) {
      buttonState = LOW;
      attachInterrupt(interruptID, ISRMethodVec[interruptID][R_INDEX],RISING);
    }
    #ifdef DEBUG
    digitalWrite(buttonLedPin, buttonState);
    #endif
    Switch(buttonState);
  }
}

void DBPI::Switch(int state){
  if (latching){
    if (state == HIGH){
      switchState = !switchState;
    }
  }
  else{
    switchState = !switchState;
  }
  #ifdef DEBUG
  if (switchState){
    digitalWrite(switchLedPin, HIGH);
  }
  else{
    digitalWrite(switchLedPin, LOW);
  }
  #endif
}
 
