#include "db.h"


void DebouncedInterrupt::Register(int interrupt_number, DebouncedInterrupt* intThisPtr){
    ISRVectorTable[interrupt_number] = intThisPtr;
}

DebouncedInterrupt* DebouncedInterrupt::ISRVectorTable[MAX_INTERRUPTS];
staticFptr DebouncedInterrupt::ISRMethodVec[MAX_INTERRUPTS][2] = {{DebouncedInterrupt::DebouncedInterrupt_0R,
								   DebouncedInterrupt::DebouncedInterrupt_0F},
								  {DebouncedInterrupt::DebouncedInterrupt_1R,
								   DebouncedInterrupt::DebouncedInterrupt_1F},
								  {DebouncedInterrupt::DebouncedInterrupt_2R,
								   DebouncedInterrupt::DebouncedInterrupt_2F},
								  {DebouncedInterrupt::DebouncedInterrupt_3R,
								   DebouncedInterrupt::DebouncedInterrupt_3F},
								  {DebouncedInterrupt::DebouncedInterrupt_4R,
								   DebouncedInterrupt::DebouncedInterrupt_4F},
								  {DebouncedInterrupt::DebouncedInterrupt_5R,
								   DebouncedInterrupt::DebouncedInterrupt_5F},
								  {DebouncedInterrupt::DebouncedInterrupt_6R,
								   DebouncedInterrupt::DebouncedInterrupt_6F},
								  {DebouncedInterrupt::DebouncedInterrupt_7R,
								   DebouncedInterrupt::DebouncedInterrupt_7F},
								  {DebouncedInterrupt::DebouncedInterrupt_8R,
								   DebouncedInterrupt::DebouncedInterrupt_8F},
								  {DebouncedInterrupt::DebouncedInterrupt_9R,
								   DebouncedInterrupt::DebouncedInterrupt_9F},
								  {DebouncedInterrupt::DebouncedInterrupt_10R,
								   DebouncedInterrupt::DebouncedInterrupt_10F},
								  {DebouncedInterrupt::DebouncedInterrupt_11R,
								   DebouncedInterrupt::DebouncedInterrupt_11F},
								  {DebouncedInterrupt::DebouncedInterrupt_12R,
								   DebouncedInterrupt::DebouncedInterrupt_12F},
								  {DebouncedInterrupt::DebouncedInterrupt_13R,
								   DebouncedInterrupt::DebouncedInterrupt_13F},
								  {DebouncedInterrupt::DebouncedInterrupt_14R,
								   DebouncedInterrupt::DebouncedInterrupt_14F},
								  {DebouncedInterrupt::DebouncedInterrupt_15R,
								   DebouncedInterrupt::DebouncedInterrupt_15F},
								  {DebouncedInterrupt::DebouncedInterrupt_16R,
								   DebouncedInterrupt::DebouncedInterrupt_16F},
								  {DebouncedInterrupt::DebouncedInterrupt_17R,
								   DebouncedInterrupt::DebouncedInterrupt_17F},
								  {DebouncedInterrupt::DebouncedInterrupt_18R,
								   DebouncedInterrupt::DebouncedInterrupt_18F},
								  {DebouncedInterrupt::DebouncedInterrupt_19R,
								   DebouncedInterrupt::DebouncedInterrupt_19F},
								  {DebouncedInterrupt::DebouncedInterrupt_20R,
								   DebouncedInterrupt::DebouncedInterrupt_20F},
								  {DebouncedInterrupt::DebouncedInterrupt_21R,
								   DebouncedInterrupt::DebouncedInterrupt_21F},
								  {DebouncedInterrupt::DebouncedInterrupt_22R,
								   DebouncedInterrupt::DebouncedInterrupt_22F},
								  {DebouncedInterrupt::DebouncedInterrupt_23R,
								   DebouncedInterrupt::DebouncedInterrupt_23F},
								  {DebouncedInterrupt::DebouncedInterrupt_24R,
								   DebouncedInterrupt::DebouncedInterrupt_24F},
								  {DebouncedInterrupt::DebouncedInterrupt_25R,
								   DebouncedInterrupt::DebouncedInterrupt_25F},
								  {DebouncedInterrupt::DebouncedInterrupt_26R,
								   DebouncedInterrupt::DebouncedInterrupt_26F},
								  {DebouncedInterrupt::DebouncedInterrupt_27R,
								   DebouncedInterrupt::DebouncedInterrupt_27F},
								  {DebouncedInterrupt::DebouncedInterrupt_28R,
								   DebouncedInterrupt::DebouncedInterrupt_28F},
								  {DebouncedInterrupt::DebouncedInterrupt_29R,
								   DebouncedInterrupt::DebouncedInterrupt_29F},
								  {DebouncedInterrupt::DebouncedInterrupt_30R,
								   DebouncedInterrupt::DebouncedInterrupt_30F},
								  {DebouncedInterrupt::DebouncedInterrupt_31R,
								   DebouncedInterrupt::DebouncedInterrupt_31F},
								  {DebouncedInterrupt::DebouncedInterrupt_32R,
								   DebouncedInterrupt::DebouncedInterrupt_32F},
								  {DebouncedInterrupt::DebouncedInterrupt_33R,
								   DebouncedInterrupt::DebouncedInterrupt_33F},
								  {DebouncedInterrupt::DebouncedInterrupt_34R,
								   DebouncedInterrupt::DebouncedInterrupt_34F},
								  {DebouncedInterrupt::DebouncedInterrupt_35R,
								   DebouncedInterrupt::DebouncedInterrupt_35F},
								  {DebouncedInterrupt::DebouncedInterrupt_36R,
								   DebouncedInterrupt::DebouncedInterrupt_36F},
								  {DebouncedInterrupt::DebouncedInterrupt_37R,
								   DebouncedInterrupt::DebouncedInterrupt_37F},
								  {DebouncedInterrupt::DebouncedInterrupt_38R,
								   DebouncedInterrupt::DebouncedInterrupt_38F},
								  {DebouncedInterrupt::DebouncedInterrupt_39R,
								   DebouncedInterrupt::DebouncedInterrupt_39F},
								  {DebouncedInterrupt::DebouncedInterrupt_40R,
								   DebouncedInterrupt::DebouncedInterrupt_40F},
								  {DebouncedInterrupt::DebouncedInterrupt_41R,
								   DebouncedInterrupt::DebouncedInterrupt_41F},
								  {DebouncedInterrupt::DebouncedInterrupt_42R,
								   DebouncedInterrupt::DebouncedInterrupt_42F},
								  {DebouncedInterrupt::DebouncedInterrupt_43R,
								   DebouncedInterrupt::DebouncedInterrupt_43F},
								  {DebouncedInterrupt::DebouncedInterrupt_44R,
								   DebouncedInterrupt::DebouncedInterrupt_44F},
								  {DebouncedInterrupt::DebouncedInterrupt_45R,
								   DebouncedInterrupt::DebouncedInterrupt_45F},
								  {DebouncedInterrupt::DebouncedInterrupt_46R,
								   DebouncedInterrupt::DebouncedInterrupt_46F},
								  {DebouncedInterrupt::DebouncedInterrupt_47R,
								   DebouncedInterrupt::DebouncedInterrupt_47F},
								  {DebouncedInterrupt::DebouncedInterrupt_48R,
								   DebouncedInterrupt::DebouncedInterrupt_48F},
								  {DebouncedInterrupt::DebouncedInterrupt_49R,
								   DebouncedInterrupt::DebouncedInterrupt_49F},
								  {DebouncedInterrupt::DebouncedInterrupt_50R,
								   DebouncedInterrupt::DebouncedInterrupt_50F},
								  {DebouncedInterrupt::DebouncedInterrupt_51R,
								   DebouncedInterrupt::DebouncedInterrupt_51F},
								  {DebouncedInterrupt::DebouncedInterrupt_52R,
								   DebouncedInterrupt::DebouncedInterrupt_52F},
								  {DebouncedInterrupt::DebouncedInterrupt_53R,
								   DebouncedInterrupt::DebouncedInterrupt_53F}};

void DebouncedInterrupt::DebouncedInterrupt_0R(void){
  ISRVectorTable[0]->ISR_R();
}
void DebouncedInterrupt::DebouncedInterrupt_0F(void){
  ISRVectorTable[0]->ISR_F();
}
void DebouncedInterrupt::DebouncedInterrupt_1R(void){
  ISRVectorTable[1]->ISR_R();
}
void DebouncedInterrupt::DebouncedInterrupt_1F(void){
  ISRVectorTable[1]->ISR_F();
}
void DebouncedInterrupt::DebouncedInterrupt_2R(void){
  ISRVectorTable[2]->ISR_R();
}
void DebouncedInterrupt::DebouncedInterrupt_2F(void){
  ISRVectorTable[2]->ISR_F();
}
void DebouncedInterrupt::DebouncedInterrupt_3R(void){
  ISRVectorTable[3]->ISR_R();
}
void DebouncedInterrupt::DebouncedInterrupt_3F(void){
  ISRVectorTable[3]->ISR_F();
}
void DebouncedInterrupt::DebouncedInterrupt_4R(void){
  ISRVectorTable[4]->ISR_R();
}
void DebouncedInterrupt::DebouncedInterrupt_4F(void){
  ISRVectorTable[4]->ISR_F();
}
void DebouncedInterrupt::DebouncedInterrupt_5R(void){
  ISRVectorTable[5]->ISR_R();
}
void DebouncedInterrupt::DebouncedInterrupt_5F(void){
  ISRVectorTable[5]->ISR_F();
}
void DebouncedInterrupt::DebouncedInterrupt_6R(void){
  ISRVectorTable[6]->ISR_R();
}
void DebouncedInterrupt::DebouncedInterrupt_6F(void){
  ISRVectorTable[6]->ISR_F();
}
void DebouncedInterrupt::DebouncedInterrupt_7R(void){
  ISRVectorTable[7]->ISR_R();
}
void DebouncedInterrupt::DebouncedInterrupt_7F(void){
  ISRVectorTable[7]->ISR_F();
}
void DebouncedInterrupt::DebouncedInterrupt_8R(void){
  ISRVectorTable[8]->ISR_R();
}
void DebouncedInterrupt::DebouncedInterrupt_8F(void){
  ISRVectorTable[8]->ISR_F();
}
void DebouncedInterrupt::DebouncedInterrupt_9R(void){
  ISRVectorTable[9]->ISR_R();
}
void DebouncedInterrupt::DebouncedInterrupt_9F(void){
  ISRVectorTable[9]->ISR_F();
}
void DebouncedInterrupt::DebouncedInterrupt_10R(void){
  ISRVectorTable[10]->ISR_R();
}
void DebouncedInterrupt::DebouncedInterrupt_10F(void){
  ISRVectorTable[10]->ISR_F();
}
void DebouncedInterrupt::DebouncedInterrupt_11R(void){
  ISRVectorTable[11]->ISR_R();
}
void DebouncedInterrupt::DebouncedInterrupt_11F(void){
  ISRVectorTable[11]->ISR_F();
}
void DebouncedInterrupt::DebouncedInterrupt_12R(void){
  ISRVectorTable[12]->ISR_R();
}
void DebouncedInterrupt::DebouncedInterrupt_12F(void){
  ISRVectorTable[12]->ISR_F();
}
void DebouncedInterrupt::DebouncedInterrupt_13R(void){
  ISRVectorTable[13]->ISR_R();
}
void DebouncedInterrupt::DebouncedInterrupt_13F(void){
  ISRVectorTable[13]->ISR_F();
}
void DebouncedInterrupt::DebouncedInterrupt_14R(void){
  ISRVectorTable[14]->ISR_R();
}
void DebouncedInterrupt::DebouncedInterrupt_14F(void){
  ISRVectorTable[14]->ISR_F();
}
void DebouncedInterrupt::DebouncedInterrupt_15R(void){
  ISRVectorTable[15]->ISR_R();
}
void DebouncedInterrupt::DebouncedInterrupt_15F(void){
  ISRVectorTable[15]->ISR_F();
}
void DebouncedInterrupt::DebouncedInterrupt_16R(void){
  ISRVectorTable[16]->ISR_R();
}
void DebouncedInterrupt::DebouncedInterrupt_16F(void){
  ISRVectorTable[16]->ISR_F();
}
void DebouncedInterrupt::DebouncedInterrupt_17R(void){
  ISRVectorTable[17]->ISR_R();
}
void DebouncedInterrupt::DebouncedInterrupt_17F(void){
  ISRVectorTable[17]->ISR_F();
}
void DebouncedInterrupt::DebouncedInterrupt_18R(void){
  ISRVectorTable[18]->ISR_R();
}
void DebouncedInterrupt::DebouncedInterrupt_18F(void){
  ISRVectorTable[18]->ISR_F();
}
void DebouncedInterrupt::DebouncedInterrupt_19R(void){
  ISRVectorTable[19]->ISR_R();
}
void DebouncedInterrupt::DebouncedInterrupt_19F(void){
  ISRVectorTable[19]->ISR_F();
}
void DebouncedInterrupt::DebouncedInterrupt_20R(void){
  ISRVectorTable[20]->ISR_R();
}
void DebouncedInterrupt::DebouncedInterrupt_20F(void){
  ISRVectorTable[20]->ISR_F();
}
void DebouncedInterrupt::DebouncedInterrupt_21R(void){
  ISRVectorTable[21]->ISR_R();
}
void DebouncedInterrupt::DebouncedInterrupt_21F(void){
  ISRVectorTable[21]->ISR_F();
}
void DebouncedInterrupt::DebouncedInterrupt_22R(void){
  ISRVectorTable[22]->ISR_R();
}
void DebouncedInterrupt::DebouncedInterrupt_22F(void){
  ISRVectorTable[22]->ISR_F();
}
void DebouncedInterrupt::DebouncedInterrupt_23R(void){
  ISRVectorTable[23]->ISR_R();
}
void DebouncedInterrupt::DebouncedInterrupt_23F(void){
  ISRVectorTable[23]->ISR_F();
}
void DebouncedInterrupt::DebouncedInterrupt_24R(void){
  ISRVectorTable[24]->ISR_R();
}
void DebouncedInterrupt::DebouncedInterrupt_24F(void){
  ISRVectorTable[24]->ISR_F();
}
void DebouncedInterrupt::DebouncedInterrupt_25R(void){
  ISRVectorTable[25]->ISR_R();
}
void DebouncedInterrupt::DebouncedInterrupt_25F(void){
  ISRVectorTable[25]->ISR_F();
}
void DebouncedInterrupt::DebouncedInterrupt_26R(void){
  ISRVectorTable[26]->ISR_R();
}
void DebouncedInterrupt::DebouncedInterrupt_26F(void){
  ISRVectorTable[26]->ISR_F();
}
void DebouncedInterrupt::DebouncedInterrupt_27R(void){
  ISRVectorTable[27]->ISR_R();
}
void DebouncedInterrupt::DebouncedInterrupt_27F(void){
  ISRVectorTable[27]->ISR_F();
}
void DebouncedInterrupt::DebouncedInterrupt_28R(void){
  ISRVectorTable[28]->ISR_R();
}
void DebouncedInterrupt::DebouncedInterrupt_28F(void){
  ISRVectorTable[28]->ISR_F();
}
void DebouncedInterrupt::DebouncedInterrupt_29R(void){
  ISRVectorTable[29]->ISR_R();
}
void DebouncedInterrupt::DebouncedInterrupt_29F(void){
  ISRVectorTable[29]->ISR_F();
}
void DebouncedInterrupt::DebouncedInterrupt_30R(void){
  ISRVectorTable[30]->ISR_R();
}
void DebouncedInterrupt::DebouncedInterrupt_30F(void){
  ISRVectorTable[30]->ISR_F();
}
void DebouncedInterrupt::DebouncedInterrupt_31R(void){
  ISRVectorTable[31]->ISR_R();
}
void DebouncedInterrupt::DebouncedInterrupt_31F(void){
  ISRVectorTable[31]->ISR_F();
}
void DebouncedInterrupt::DebouncedInterrupt_32R(void){
  ISRVectorTable[32]->ISR_R();
}
void DebouncedInterrupt::DebouncedInterrupt_32F(void){
  ISRVectorTable[32]->ISR_F();
}
void DebouncedInterrupt::DebouncedInterrupt_33R(void){
  ISRVectorTable[33]->ISR_R();
}
void DebouncedInterrupt::DebouncedInterrupt_33F(void){
  ISRVectorTable[33]->ISR_F();
}
void DebouncedInterrupt::DebouncedInterrupt_34R(void){
  ISRVectorTable[34]->ISR_R();
}
void DebouncedInterrupt::DebouncedInterrupt_34F(void){
  ISRVectorTable[34]->ISR_F();
}
void DebouncedInterrupt::DebouncedInterrupt_35R(void){
  ISRVectorTable[35]->ISR_R();
}
void DebouncedInterrupt::DebouncedInterrupt_35F(void){
  ISRVectorTable[35]->ISR_F();
}
void DebouncedInterrupt::DebouncedInterrupt_36R(void){
  ISRVectorTable[36]->ISR_R();
}
void DebouncedInterrupt::DebouncedInterrupt_36F(void){
  ISRVectorTable[36]->ISR_F();
}
void DebouncedInterrupt::DebouncedInterrupt_37R(void){
  ISRVectorTable[37]->ISR_R();
}
void DebouncedInterrupt::DebouncedInterrupt_37F(void){
  ISRVectorTable[37]->ISR_F();
}
void DebouncedInterrupt::DebouncedInterrupt_38R(void){
  ISRVectorTable[38]->ISR_R();
}
void DebouncedInterrupt::DebouncedInterrupt_38F(void){
  ISRVectorTable[38]->ISR_F();
}
void DebouncedInterrupt::DebouncedInterrupt_39R(void){
  ISRVectorTable[39]->ISR_R();
}
void DebouncedInterrupt::DebouncedInterrupt_39F(void){
  ISRVectorTable[39]->ISR_F();
}
void DebouncedInterrupt::DebouncedInterrupt_40R(void){
  ISRVectorTable[40]->ISR_R();
}
void DebouncedInterrupt::DebouncedInterrupt_40F(void){
  ISRVectorTable[40]->ISR_F();
}
void DebouncedInterrupt::DebouncedInterrupt_41R(void){
  ISRVectorTable[41]->ISR_R();
}
void DebouncedInterrupt::DebouncedInterrupt_41F(void){
  ISRVectorTable[41]->ISR_F();
}
void DebouncedInterrupt::DebouncedInterrupt_42R(void){
  ISRVectorTable[42]->ISR_R();
}
void DebouncedInterrupt::DebouncedInterrupt_42F(void){
  ISRVectorTable[42]->ISR_F();
}
void DebouncedInterrupt::DebouncedInterrupt_43R(void){
  ISRVectorTable[43]->ISR_R();
}
void DebouncedInterrupt::DebouncedInterrupt_43F(void){
  ISRVectorTable[43]->ISR_F();
}
void DebouncedInterrupt::DebouncedInterrupt_44R(void){
  ISRVectorTable[44]->ISR_R();
}
void DebouncedInterrupt::DebouncedInterrupt_44F(void){
  ISRVectorTable[44]->ISR_F();
}
void DebouncedInterrupt::DebouncedInterrupt_45R(void){
  ISRVectorTable[45]->ISR_R();
}
void DebouncedInterrupt::DebouncedInterrupt_45F(void){
  ISRVectorTable[45]->ISR_F();
}
void DebouncedInterrupt::DebouncedInterrupt_46R(void){
  ISRVectorTable[46]->ISR_R();
}
void DebouncedInterrupt::DebouncedInterrupt_46F(void){
  ISRVectorTable[46]->ISR_F();
}
void DebouncedInterrupt::DebouncedInterrupt_47R(void){
  ISRVectorTable[47]->ISR_R();
}
void DebouncedInterrupt::DebouncedInterrupt_47F(void){
  ISRVectorTable[47]->ISR_F();
}
void DebouncedInterrupt::DebouncedInterrupt_48R(void){
  ISRVectorTable[48]->ISR_R();
}
void DebouncedInterrupt::DebouncedInterrupt_48F(void){
  ISRVectorTable[48]->ISR_F();
}
void DebouncedInterrupt::DebouncedInterrupt_49R(void){
  ISRVectorTable[49]->ISR_R();
}
void DebouncedInterrupt::DebouncedInterrupt_49F(void){
  ISRVectorTable[49]->ISR_F();
}
void DebouncedInterrupt::DebouncedInterrupt_50R(void){
  ISRVectorTable[50]->ISR_R();
}
void DebouncedInterrupt::DebouncedInterrupt_50F(void){
  ISRVectorTable[50]->ISR_F();
}
void DebouncedInterrupt::DebouncedInterrupt_51R(void){
  ISRVectorTable[51]->ISR_R();
}
void DebouncedInterrupt::DebouncedInterrupt_51F(void){
  ISRVectorTable[51]->ISR_F();
}
void DebouncedInterrupt::DebouncedInterrupt_52R(void){
  ISRVectorTable[52]->ISR_R();
}
void DebouncedInterrupt::DebouncedInterrupt_52F(void){
  ISRVectorTable[52]->ISR_F();
}
void DebouncedInterrupt::DebouncedInterrupt_53R(void){
  ISRVectorTable[53]->ISR_R();
}
void DebouncedInterrupt::DebouncedInterrupt_53F(void){
  ISRVectorTable[53]->ISR_F();
}
void DebouncedInterrupt::DebouncedInterrupt_54R(void){
  ISRVectorTable[54]->ISR_R();
}
void DebouncedInterrupt::DebouncedInterrupt_54F(void){
  ISRVectorTable[54]->ISR_F();
}
