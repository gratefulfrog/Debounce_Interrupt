# Debounce Interrupt

Provides a reliable mechanism to debounce input that is to be used in the arduino attachInterrupt call.

I have adpated this from code from a publication by Fritzing.com 
https://fritzing.org/projects/debounce-interrupt


# Usage

This library provides a means to know when an interrupt is triggered on a bouncy pin. It can react to RISING or FALLING but not CHANGE.

It supports both PULL UP and PULL DOWN input pins.

The pin trigger is detected by observing the state of a boolean provided as input to the library.

The file debug.h contains #define DEBUG which if set provides debugging output. If enabled, LEDS can be connected, with appropriate resistors, to pins 12 and 13 to show the various states.


##Example

```
#include "dbpi.h"

volatile boolean switchState = false; // this is where we can see if the interrupt has been triggered
const int interruptPin = 2;          // this is the input pin that will trigger the interrupt.

DBPI *dbpi;

void setup() {
  Serial.begin(115200);
  while(!Serial); 

  // all we need to do is create an instance of the DBPI class. All the mechanics are encapsuated in the class
  // constructor arguments are: 
  // PIN ; pin number as per board specification
  // PULL :  either DEBOUNCE_PULLDOWN or DEBOUNCE_PULLUP, 
  // a reference to a volatile boolean variable that will hold true or false interrupt flag, 
  // a boolean latching_or_not
  // 
  // Here we create a PULLDOWN and latching interrupt on pin 2? the variable 'switchState' will contain true of false 
  // depending on the triggering. In this present case we are using LATCHING so each trigger toggles the state.
  // had we set latching to 'false' the variable 'switchState' would be true while the pin was high, and false when
  // it was released (due to the PULL DOWN).
  dbpi = new DBPI(interruptPin,DEBOUNCE_PULLDOWN,switchState,true); // last arg is latching defaults to false 
  
  Serial.println("Starting up!");
}

// example code counts the button changes of state
void loop(){
  static bool localSwitchState = switchState;
  static int count = 0;
  if (switchState != localSwitchState){
    localSwitchState = switchState;
    String s = String (count++) + " : " +String(switchState ? "On" : "Off" ) ;
    Serial.println(s);
  }
}
/*   Serial Monitor Output
pin: 2
pull: 0
interruptID: 0
Starting up!
0 : On
1 : Off
2 : On
3 : Off
4 : On
5 : Off
6 : On
7 : Off
8 : On
9 : Off
10 : On
11 : Off
*/
```

