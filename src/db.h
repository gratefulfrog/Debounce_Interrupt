#ifndef DEBOUNCE_INTERRUPT_PIN_H
#define DEBOUNCE_INTERRUPT_PIN_H

#include <Arduino.h>
#include "debug.h"

#define MAX_INTERRUPTS    (54)

#define R_INDEX (0)
#define F_INDEX (1)

class DebouncedInterrupt;

typedef void (*staticFptr)();

class DebouncedInterrupt{
  public:
    DebouncedInterrupt(void){}
    static void Register(int interrupt_numberber, DebouncedInterrupt* intThisPtr);

    virtual void ISR_R(void) = 0;
    virtual void ISR_F(void) = 0;

 protected:
    static DebouncedInterrupt* ISRVectorTable[MAX_INTERRUPTS];
    static staticFptr ISRMethodVec[MAX_INTERRUPTS][2];

    // wrapper functions to ISR()
    static void DebouncedInterrupt_0R(void);
    static void DebouncedInterrupt_0F(void);
    static void DebouncedInterrupt_1R(void);
    static void DebouncedInterrupt_1F(void);
    static void DebouncedInterrupt_2R(void);
    static void DebouncedInterrupt_2F(void);
    static void DebouncedInterrupt_3R(void);
    static void DebouncedInterrupt_3F(void);
    static void DebouncedInterrupt_4R(void);
    static void DebouncedInterrupt_4F(void);
    static void DebouncedInterrupt_5R(void);
    static void DebouncedInterrupt_5F(void);
    static void DebouncedInterrupt_6R(void);
    static void DebouncedInterrupt_6F(void);
    static void DebouncedInterrupt_7R(void);
    static void DebouncedInterrupt_7F(void);
    static void DebouncedInterrupt_8R(void);
    static void DebouncedInterrupt_8F(void);
    static void DebouncedInterrupt_9R(void);
    static void DebouncedInterrupt_9F(void);
    static void DebouncedInterrupt_10R(void);
    static void DebouncedInterrupt_10F(void);
    static void DebouncedInterrupt_11R(void);
    static void DebouncedInterrupt_11F(void);
    static void DebouncedInterrupt_12R(void);
    static void DebouncedInterrupt_12F(void);
    static void DebouncedInterrupt_13R(void);
    static void DebouncedInterrupt_13F(void);
    static void DebouncedInterrupt_14R(void);
    static void DebouncedInterrupt_14F(void);
    static void DebouncedInterrupt_15R(void);
    static void DebouncedInterrupt_15F(void);
    static void DebouncedInterrupt_16R(void);
    static void DebouncedInterrupt_16F(void);
    static void DebouncedInterrupt_17R(void);
    static void DebouncedInterrupt_17F(void);
    static void DebouncedInterrupt_18R(void);
    static void DebouncedInterrupt_18F(void);
    static void DebouncedInterrupt_19R(void);
    static void DebouncedInterrupt_19F(void);
    static void DebouncedInterrupt_20R(void);
    static void DebouncedInterrupt_20F(void);
    static void DebouncedInterrupt_21R(void);
    static void DebouncedInterrupt_21F(void);
    static void DebouncedInterrupt_22R(void);
    static void DebouncedInterrupt_22F(void);
    static void DebouncedInterrupt_23R(void);
    static void DebouncedInterrupt_23F(void);
    static void DebouncedInterrupt_24R(void);
    static void DebouncedInterrupt_24F(void);
    static void DebouncedInterrupt_25R(void);
    static void DebouncedInterrupt_25F(void);
    static void DebouncedInterrupt_26R(void);
    static void DebouncedInterrupt_26F(void);
    static void DebouncedInterrupt_27R(void);
    static void DebouncedInterrupt_27F(void);
    static void DebouncedInterrupt_28R(void);
    static void DebouncedInterrupt_28F(void);
    static void DebouncedInterrupt_29R(void);
    static void DebouncedInterrupt_29F(void);
    static void DebouncedInterrupt_30R(void);
    static void DebouncedInterrupt_30F(void);
    static void DebouncedInterrupt_31R(void);
    static void DebouncedInterrupt_31F(void);
    static void DebouncedInterrupt_32R(void);
    static void DebouncedInterrupt_32F(void);
    static void DebouncedInterrupt_33R(void);
    static void DebouncedInterrupt_33F(void);
    static void DebouncedInterrupt_34R(void);
    static void DebouncedInterrupt_34F(void);
    static void DebouncedInterrupt_35R(void);
    static void DebouncedInterrupt_35F(void);
    static void DebouncedInterrupt_36R(void);
    static void DebouncedInterrupt_36F(void);
    static void DebouncedInterrupt_37R(void);
    static void DebouncedInterrupt_37F(void);
    static void DebouncedInterrupt_38R(void);
    static void DebouncedInterrupt_38F(void);
    static void DebouncedInterrupt_39R(void);
    static void DebouncedInterrupt_39F(void);
    static void DebouncedInterrupt_40R(void);
    static void DebouncedInterrupt_40F(void);
    static void DebouncedInterrupt_41R(void);
    static void DebouncedInterrupt_41F(void);
    static void DebouncedInterrupt_42R(void);
    static void DebouncedInterrupt_42F(void);
    static void DebouncedInterrupt_43R(void);
    static void DebouncedInterrupt_43F(void);
    static void DebouncedInterrupt_44R(void);
    static void DebouncedInterrupt_44F(void);
    static void DebouncedInterrupt_45R(void);
    static void DebouncedInterrupt_45F(void);
    static void DebouncedInterrupt_46R(void);
    static void DebouncedInterrupt_46F(void);
    static void DebouncedInterrupt_47R(void);
    static void DebouncedInterrupt_47F(void);
    static void DebouncedInterrupt_48R(void);
    static void DebouncedInterrupt_48F(void);
    static void DebouncedInterrupt_49R(void);
    static void DebouncedInterrupt_49F(void);
    static void DebouncedInterrupt_50R(void);
    static void DebouncedInterrupt_50F(void);
    static void DebouncedInterrupt_51R(void);
    static void DebouncedInterrupt_51F(void);
    static void DebouncedInterrupt_52R(void);
    static void DebouncedInterrupt_52F(void);
    static void DebouncedInterrupt_53R(void);
    static void DebouncedInterrupt_53F(void);
    static void DebouncedInterrupt_54R(void);
    static void DebouncedInterrupt_54F(void);
};

#endif
