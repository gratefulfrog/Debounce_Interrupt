/*
This code is based on a publication by Fritzing.com 
https://fritzing.org/projects/debounce-interrupt

It is a library for the creation of debounced buttons to be used as itnerrup triggers.

The configuration parameters are:

interruptPin :
* PULLED UP   : the the internal arduino PULLUP resistor is activated
* PULLED DOWN : an external pull-down resistor must be incorporated into the circuit.
* 
switchState : momentary or latching, . 
* Momentary : pushing the button sets switchState to true while releasing it sets switchstate to false 
* Latching  : the first press of the button sets switchState true, a second press sets switchState to false, and so on.


Since the millis() function does not update in an interrupt service routine and since calling 
the interrupt on CHANGE does not indicate the rising or falling state, the logic for debouncing a button is a little different here.

A button is attached to the interrupt 0 pin 2. A LED indicating the state of the button is attached to pin 13 and 
a LED indicating the state of an on-off condition (such as causing something happen or not happen in the loop() function) is attached to pin 12.

At the start of the sketch, the interrupt is attached to a routine called Rise that is fired when the button state goes from low to high. 
When this routing is called, we compare the value of millis() to the stored value of the last interrupt time. 
If the difference is less then debounceDelay, we change the state of the button and attach the interrupt to 
a service routine called Fall which is fired when the button goes from high to low.

Since the millis() function does not update during the interrupt service routine, the debounceDelay value can be very small since 
the noise created by the button will cause millis() to return values that are very close or exactly the same regardless 
of how long we spend in the service routine.
*/

#include <dbpi.h>

volatile boolean switchState = false; 
const int interruptPin = 2;

DBPI *dbpi;

void setup() {
  Serial.begin(115200);
  while(!Serial); 

  // constructor arguments are: 
  // PIN ; pin number as per board specification
  // PULL :  either DEBOUNCE_PULLDOWN or DEBOUNCE_PULLUP, 
  // a reference to a volatile boolean variable that will hold true or false interrupt flag, 
  // a boolean latching_or_not
  dbpi = new DBPI(interruptPin,DEBOUNCE_PULLDOWN,switchState,false); // last arg is latching defaults to false 
  
  Serial.println("Starting up!");
}


// example code counts the button changes of state
void loop(){
  static bool localSwitchState = switchState;
  static int count = 0;
  if (switchState != localSwitchState){
    localSwitchState = switchState;
    String s = String (count++) + " : " +String(switchState ? "On" : "Off" ) ;
    Serial.println(s);
  }
}
