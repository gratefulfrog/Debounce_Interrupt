#ifndef D_B_P_I_H
#define D_B_P_I_H

#include "db.h"

#define DEBOUNCE_PULLUP   (1)
#define DEBOUNCE_PULLDOWN (0)


class DBPI : public DebouncedInterrupt{
  protected:
    const int pin,
              pull, 
              interruptID,
              latching;

    const unsigned long debounceDelay = 5;  

    volatile int buttonState;                    
    volatile long unsigned lastDebounceTime = 0;   // the last time the interrupt was triggered

    volatile boolean &switchState;   // This is the boolean that is set to true/false on push/release, init to  false; 

    virtual void ISR_R(void);
    virtual void ISR_F(void);
    void Switch(int state);

 public:
  DBPI(int _pin,
       int _pull,
       volatile bool &_switchState,
       bool _latching = false);
};

#endif
